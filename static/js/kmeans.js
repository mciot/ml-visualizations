/* Fetch data from API */
queue().defer(d3.json, "/ml/kmeans").await(kmeans);

/* Create the kmeans voronoi diagram */
function kmeans (error, iter_data) {
	if (error) throw error;

	var data = iter_data['iter-data'];

	var width = 1000,
		height = 600;
	var svg = d3.select(".container").append("svg").attr("height", height).attr("width", width);

	// Min and max values for center coordinates 
	var xmax = d3.max(iter_data.data, function (d) { return d[0]; });
	var xmin = d3.min(iter_data.data, function (d) { return d[0]; });
	var ymax = d3.max(iter_data.data, function (d) { return d[1] });
	var ymin = d3.min(iter_data.data, function (d) { return d[1]; });

	// X scale for centers 
	var xscale = d3.scale.linear().domain([xmin, xmax]).range([0, width])

	// Y scale for centers 
	var yscale = d3.scale.linear().domain([ymin, ymax]).range([height, 0]);

	// Plot the data points 
	// var points = svg.selectAll(".point")
	// 	.data(iter_data.data)
	// 	.enter()
	// 	.append("circle")
	// 	.attr("r", 2)
	// 	.attr("fill", "red")
	// 	.attr("cx", function (d) { return xscale(d[0]); })
	// 	.attr("cy", function (d) { return yscale(d[1]); });

	var center;

	// Each iteration of the algorithm - essentially a refresh function
	var iter = function (labels, centers, inertia, xscale, yscale) {
		// Plot centers 
		center = svg.selectAll(".center").data(centers, function (d) { return d.name; });
		
		console.log('update:', center);
		console.log('enter:', center.enter());
		console.log('exit:', center.exit());

		center.enter().append("circle").attr("r", 5);
		
		// Update center positions 
		center.attr("cx", function (d) { return xscale(d.coords[0]); })
			.attr("cy", function (d) { return yscale(d.coords[1]); });
		
		// Remove previous centers 
		center.exit().remove();
	};

	// Step through the iterations 
	//var centers;
	var i = 0; 
	setInterval(function () {
		if (i < data.length) {
			centers = data[i].centers.map(function (c, j) { return {'name': i + ',' + j, 'coords': c}; });
			iter(data[i].labels, centers, data[i].inertia, xscale, yscale);
		}
			
		//else i = -1;
		else return;

		i++;
	}, 500);

}