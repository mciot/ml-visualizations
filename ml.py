import json
import numpy as np
from utils import serializablize

from flask import Flask
from flask import render_template

from sklearn import metrics
from sklearn.cluster import KMeans
from sklearn.datasets import load_digits
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale

app = Flask(__name__)

# Template rendering methods 

@app.route('/')
def index():
	return 'Hello World!'

@app.route('/kmeans')
def kmeans():
	return render_template('kmeans.html')


# API methods - return JSON with algorithm information

@app.route('/ml/kmeans')
def _kmeans():
	np.random.seed(42)
	digits = load_digits()
	data = scale(digits.data)

	n_samples, n_features = data.shape
	n_digits = len(np.unique(digits.target))
	labels = digits.target

	# plus = KMeans(init='k-means++', n_clusters=n_digits, n_init=10, iter_data=[])
	# plus.fit(data)
	# rand = KMeans(init='random', n_clusters=n_digits, n_init=10)
	# rand.fit(data)
	# pca = PCA(n_components=n_digits).fit(data)
	# kpca = KMeans(init=pca.components_, n_clusters=n_digits, n_init=1)
	# kpca.fit(data)

	reduced_data = PCA(n_components=2).fit_transform(data)
	kmeans = KMeans(init='k-means++', n_clusters=n_digits, n_init=10, iter_data=[])
	kmeans.fit(reduced_data)

	iter_data = {'iter-data': serializablize(kmeans.iter_data), 'data': serializablize(reduced_data)}
	return json.dumps(iter_data)


if __name__ == '__main__':
	app.run(debug=True)
