from time import time
import numpy as np

from sklearn import metrics
from sklearn.cluster import KMeans
from sklearn.datasets import load_digits
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale

np.random.seed(42)

digits = load_digits()
data = scale(digits.data)

n_samples, n_features = data.shape
n_digits = len(np.unique(digits.target))
labels = digits.target

sample_size = 300

def bench_k_means(estimator, name, data):
    t0 = time()
    estimator.fit(data)
    print('% 9s   %.2fs    %i   %.3f   %.3f   %.3f   %.3f   %.3f    %.3f'
          % (name, (time() - t0), estimator.inertia_,
             metrics.homogeneity_score(labels, estimator.labels_),
             metrics.completeness_score(labels, estimator.labels_),
             metrics.v_measure_score(labels, estimator.labels_),
             metrics.adjusted_rand_score(labels, estimator.labels_),
             metrics.adjusted_mutual_info_score(labels,  estimator.labels_),
             metrics.silhouette_score(data, estimator.labels_,
                                      metric='euclidean',
                                      sample_size=sample_size)))

plus = KMeans(init='k-means++', n_clusters=n_digits, n_init=10, iter_data=[])
plus.fit(data)

#print plus.iter_data[0]
print type(plus.iter_data[0]['labels'])
print type(plus.iter_data[0]['inertia'])
print type(plus.iter_data[0]['centers'])
print type(plus.iter_data[0]['labels'][0])

print serializablize(plus.iter_data)

# rand = KMeans(init='random', n_clusters=n_digits, n_init=10)

# pca = PCA(n_components=n_digits).fit(data)
# kpca = KMeans(init=pca.components_, n_clusters=n_digits, n_init=1)




