''' Some general utility methods ''' 

import numpy as np

# Make json-serializable, i.e., no numpy stuff 
def serializablize(obj):
	if type(obj) == np.ndarray or type(obj) == list:
		return [serializablize(x) for x in obj]
	if type(obj) == np.float64:
		return float(obj)
	if type(obj) == np.int32:
		return int(obj)
	if type(obj) == dict:
		for k, v in obj.iteritems():
			obj[k] = serializablize(v)
		return obj
	else:
		return obj
	